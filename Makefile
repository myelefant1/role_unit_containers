CI_REGISTRY ?= registry.gitlab.com
CI_PROJECT_PATH ?= role_unit/role_unit_containers
CI_REGISTRY_IMAGE ?= $(CI_REGISTRY)/$(CI_PROJECT_PATH)
TARGETS = $(wildcard archlinux ubuntu[0-9]* debian[0-9]* centos[0-9]* build)
CENTOS_TARGETS = $(patsubst %,%_cached,$(wildcard centos[0-9]*))

containers: $(TARGETS)

$(TARGETS): login
	echo $@
	docker build -t ${CI_REGISTRY_IMAGE}:$@ -f $@/dockerfile $@
	test "${CI_COMMIT_REF_NAME}" != "master" || docker push ${CI_REGISTRY_IMAGE}:$@
	echo $(CI_REGISTRY_IMAGE)

$(CENTOS_TARGETS): login
	echo $@
	docker build --build-arg CENTOS_VERSION=$$(echo $@ | sed 's/_cached$$//') -t ${CI_REGISTRY_IMAGE}:$@ -f centos_cached/dockerfile centos_cached
	test "${CI_COMMIT_REF_NAME}" != "master" || docker push ${CI_REGISTRY_IMAGE}:$@

login:
	test -z "${CI}" || echo "${CI_REGISTRY_PASSWORD}" | docker login -u ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY}

.PHONY: $(TARGETS)
